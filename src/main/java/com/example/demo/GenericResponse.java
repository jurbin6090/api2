package com.example.demo;


public class GenericResponse {
    private long id;
    private String message;

    public long getId() { return id; }
    public String getMessage() { return message; }

    public void setId(long val) { id = val; }
    public void setMessage(String val) { message = val; }

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("id: " + id);
        str.append(", message: " + message);
        return str.toString();
    }
}
